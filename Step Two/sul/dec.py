﻿import zlib
from Crypto import Random
from Crypto.Cipher import AES
import base64
import binascii





time_epoch = 1440957600
php = "mt_srand(%d); for ($i=0;$i<128/8;$i++) echo sprintf(\"%02x\", mt_rand(0,255)); echo \"</br>\";"

file = open("test.php", "w");
for i in range(0, 60 * 60):
    php_new =  "mt_srand(%d);" % time_epoch + "for ($i=0;$i<128/8;$i++) echo sprintf(\"%02x\", mt_rand(0,255)); echo \"</br>\";\n"
    time_epoch = time_epoch + 1
    file.write(php_new)
file.close()

keys = open("keys.txt").read().splitlines()
hex_keys = []
for key in keys :
    hex_keys.append(binascii.a2b_hex(key))

data_enc = open("solveme-enc.jpg", "rb").read()

iv = binascii.a2b_hex("00000000000000000000000000000000")
i = 0
for key in hex_keys :
    cipher = AES.new(key, AES.MODE_ECB, iv)
    data_decrypted = cipher.decrypt(data_enc)
    file = open("dec_%d.jpg" % i, "wb")
    file.write(data_decrypted)
    file.close()
    i = i + 1