---
--- Table definitions
---

CREATE TABLE [dbo].[access] (
  [aid] [smallint] NULL ,
  [mask] [varchar] (255) NOT NULL ,
  [type] [varchar] (255) NOT NULL ,
  [status] [smallint] NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[accesslog] (
  [nid] [numeric](11, 0) NULL ,
  [url] [varchar] (255) NULL ,
  [hostname] [varchar] (128) NULL ,
  [uid] [numeric](10, 0) NULL ,
  [timestamp] [numeric](11, 0) NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[authmap] (
  [aid] [numeric](10, 0) IDENTITY NULL ,
  [uid] [int] NOT NULL ,
  [authname] [varchar] (128) NOT NULL ,
  [module] [varchar] (128) NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[blocks] (
  [module] [varchar] (64) NOT NULL ,
  [delta] [varchar] (32) NOT NULL ,
  [status] [smallint] NOT NULL ,
  [weight] [smallint] NOT NULL ,
  [region] [smallint] NOT NULL ,
  [path] [varchar] (255) NOT NULL ,
  [custom] [smallint] NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[book] (
  [nid] [numeric](4, 0) NOT NULL ,
  [parent] [int] NOT NULL ,
  [weight] [smallint] NOT NULL ,
  [format] [smallint] NULL ,
  [log] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[boxes] (
  [bid] [smallint] NULL ,
  [title] [varchar] (64) NOT NULL ,
  [body] [text] NULL ,
  [info] [varchar] (128) NOT NULL ,
  [type] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[bundle] (
  [bid] [int] NULL ,
  [title] [varchar] (255) NOT NULL ,
  [attributes] [varchar] (255) NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[cache] (
  [cid] [varchar] (255) NOT NULL ,
  [data] [text] NULL ,
  [expire] [int] NOT NULL ,
  [created] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[comments] (
  [cid] [int] NULL ,
  [pid] [int] NOT NULL ,
  [nid] [int] NOT NULL ,
  [uid] [int] NOT NULL ,
  [subject] [varchar] (64) NOT NULL ,
  [comment] [varchar] (8000) NOT NULL ,
  [hostname] [varchar] (128) NOT NULL ,
  [timestamp] [int] NOT NULL ,
  [link] [varchar] (16) NULL ,
  [score] [int] NOT NULL ,
  [status] [tinyint] NOT NULL ,
  [thread] [varchar] (255) NOT NULL ,
  [users] [varchar] (8000) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[directory] (
  [link] [varchar] (255) NOT NULL ,
  [name] [varchar] (128) NOT NULL ,
  [mail] [varchar] (128) NOT NULL ,
  [slogan] [text] NOT NULL ,
  [mission] [text] NOT NULL ,
  [timestamp] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[feed] (
  [fid] [int] NULL ,
  [title] [varchar] (255) NOT NULL ,
  [url] [varchar] (255) NOT NULL ,
  [refresh] [int] NOT NULL ,
  [timestamp] [int] NULL ,
  [attributes] [varchar] (255) NULL ,
  [link] [varchar] (255) NULL ,
  [description] [varchar] (8000) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[forum] (
  [nid] [numeric](10, 0) NOT NULL ,
  [tid] [numeric](10, 0) NOT NULL ,
  [icon] [varchar] (255) NULL ,
  [shadow] [numeric](10, 0) NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[history] (
  [uid] [int] NOT NULL ,
  [nid] [int] NOT NULL ,
  [timestamp] [int] NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[item] (
  [iid] [int] NULL ,
  [fid] [int] NOT NULL ,
  [title] [varchar] (255) NOT NULL ,
  [link] [varchar] (255) NOT NULL ,
  [author] [varchar] (255) NOT NULL ,
  [description] [text] NOT NULL ,
  [timestamp] [int] NULL ,
  [attributes] [varchar] (255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[locales] (
  [lid] [int] NULL ,
  [location] [varchar] (128) NOT NULL ,
  [string] [nvarchar] (4000) NOT NULL ,
  [da] [text] NULL ,
  [fi] [text] NULL ,
  [fr] [text] NULL ,
  [en] [text] NULL ,
  [es] [text] NULL ,
  [nl] [text] NULL ,
  [no] [text] NULL ,
  [sw] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[menu] (
  [name] [varchar] (255) NOT NULL ,
  [link] [varchar] (255) NOT NULL ,
  [help] [text] NULL ,
  [title] [varchar] (255) NOT NULL ,
  [parent] [varchar] (255) NOT NULL ,
  [weight] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[moderation_filters] (
  [fid] [numeric](10, 0) NULL ,
  [filter] [varchar] (255) NOT NULL ,
  [minimum] [smallint] NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[moderation_roles] (
  [rid] [numeric](4, 0) NOT NULL ,
  [mid] [numeric](4, 0) NOT NULL ,
  [value] [smallint] NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[moderation_votes] (
  [mid] [numeric](10, 0) NULL ,
  [vote] [varchar] (255) NULL ,
  [weight] [smallint] NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[node] (
  [nid] [numeric](10, 0) NULL ,
  [type] [varchar] (16) NOT NULL ,
  [title] [varchar] (128) NOT NULL ,
  [path] [varchar] (250) NULL ,
  [score] [int] NOT NULL ,
  [votes] [int] NOT NULL ,
  [uid] [int] NOT NULL ,
  [status] [int] NOT NULL ,
  [created] [int] NOT NULL ,
  [comment] [int] NOT NULL ,
  [promote] [int] NOT NULL ,
  [moderate] [int] NOT NULL ,
  [users] [text] NULL ,
  [attributes] [varchar] (255) NULL ,
  [teaser] [text] NOT NULL ,
  [body] [text] NOT NULL ,
  [changed] [int] NOT NULL ,
  [revisions] [text] NULL ,
  [static] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[page] (
  [nid] [numeric](10, 0) NOT NULL ,
  [link] [varchar] (128) NOT NULL ,
  [format] [smallint] NOT NULL ,
  [description] [varchar] (128) NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[url_alias] (
  [pid] numeric (10, 0) NOT NULL,
  [dst] [varchar] (128) NOT NULL,
  [src] [varchar] (128) NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[url_alias] WITH NOCHECK ADD
  CONSTRAINT [DF_[url_alias_pid] DEFAULT (0) FOR [pid],
  CONSTRAINT [DF_url_alias_dst] DEFAULT ('') FOR [dst],
  CONSTRAINT [DF_url_alias_src] DEFAULT ('') FOR [src]
GO

CREATE INDEX [url_alias_src_idx] ON [dbo].[url_alias]([src]) ON [PRIMARY]
CREATE INDEX url_alias_dst_idx ON url_alias(dst);
GO

CREATE TABLE [dbo].[permission] (
  [rid] [numeric](4, 0) NOT NULL ,
  [perm] [text] NULL ,
  [tid] [numeric](4, 0) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[poll] (
  [nid] [numeric](4, 0) NOT NULL ,
  [runtime] [int] NOT NULL ,
  [voters] [text] NOT NULL ,
  [active] [numeric](4, 0) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[poll_choices] (
  [chid] [numeric](10, 0) NULL ,
  [nid] [numeric](10, 0) NOT NULL ,
  [chtext] [varchar] (128) NOT NULL ,
  [chvotes] [int] NOT NULL ,
  [chorder] [int] NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[role] (
  [rid] [numeric](10, 0) IDENTITY (1, 1) NOT NULL ,
  [name] [varchar] (32) NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[search_index] (
  [word] [varchar] (50) NOT NULL ,
  [lno] [numeric](10, 0) NOT NULL ,
  [type] [varchar] (16) NULL ,
  [count] [numeric](10, 0) NULL
) ON [PRIMARY]
GO

CREATE TABLE sessions (
  [sid] [varchar] (32) NOT NULL,
  [uid] [int] NOT NULL,
  [hostname] [varchar] (128) NOT NULL,
  [timestamp] [integer] NOT NULL,
  [session] [text],
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[sequences] (
  [name] [varchar] (255) NOT NULL ,
  [id] [numeric](10, 0) NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[site] (
  [sid] [numeric](10, 0) NULL ,
  [name] [varchar] (128) NOT NULL ,
  [link] [varchar] (255) NOT NULL ,
  [size] [text] NOT NULL ,
  [changed] [int] NOT NULL ,
  [checked] [int] NOT NULL ,
  [feed] [varchar] (255) NOT NULL ,
  [refresh] [int] NOT NULL ,
  [threshold] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[node_counter] (
  [nid] [int] NOT NULL ,
  [totalcount] [numeric](20, 0) NOT NULL ,
  [daycount] [numeric](4, 0) NOT NULL ,
  [timestamp] [numeric](4, 0) NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[system] (
  [filename] [varchar] (255) NOT NULL ,
  [name] [varchar] (255) NOT NULL ,
  [type] [varchar] (255) NOT NULL ,
  [description] [varchar] (255) NOT NULL ,
  [status] [int] NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[term_data] (
  [tid] [numeric](10, 0) NOT NULL ,
  [vid] [numeric](10, 0) NOT NULL ,
  [name] [varchar] (255) NOT NULL ,
  [description] [text] NULL ,
  [weight] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[term_hierarchy] (
  [tid] [numeric](4, 0) NOT NULL ,
  [parent] [numeric](4, 0) NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[term_node] (
  [nid] [numeric](4, 0) NOT NULL ,
  [tid] [numeric](4, 0) NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[term_relation] (
  [tid1] [numeric](4, 0) NOT NULL ,
  [tid2] [numeric](4, 0) NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[term_synonym] (
  [tid] [numeric](10, 0) NOT NULL ,
  [name] [varchar] (255) NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[users] (
  [uid] [numeric](10, 0) NULL ,
  [name] [varchar] (60) NOT NULL ,
  [pass] [varchar] (32) NOT NULL ,
  [mail] [varchar] (64) NULL ,
  [mode] [smallint] NOT NULL ,
  [sort] [smallint] NULL ,
  [threshold] [smallint] NULL ,
  [theme] [varchar] (255) NULL ,
  [signature] [varchar] (255) NULL ,
  [timestamp] [float] NOT NULL ,
  [status] [smallint] NOT NULL ,
  [timezone] [varchar] (8) NULL ,
  [language] [char] (2) NULL ,
  [init] [varchar] (128) NULL ,
  [data] [varchar] (8000) NULL ,
  [rid] [int] NOT NULL
)
--ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[users_uid_seq] (
  [id] [int] IDENTITY (1, 1) NOT NULL ,
  [vapor] [int] NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[variable] (
  [name] [varchar] (48) NOT NULL ,
  [value] [varchar] (8000) NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[vocabulary] (
  [vid] [numeric](10, 0) NULL ,
  [name] [varchar] (255) NOT NULL ,
  [description] [text] NULL ,
  [relations] [tinyint] NOT NULL ,
  [hierarchy] [tinyint] NOT NULL ,
  [multiple] [tinyint] NOT NULL ,
  [required] [tinyint] NOT NULL ,
  [nodes] [text] NULL ,
  [weight] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[watchdog] (
  [wid] [int] IDENTITY (1, 1) NOT NULL ,
  [uid] [int] NOT NULL ,
  [type] [varchar] (16) NOT NULL ,
  [message] [text] NOT NULL ,
  [link] [varchar] (255) NOT NULL ,
  [location] [varchar] (128) NOT NULL ,
  [hostname] [varchar] (128) NOT NULL ,
  [timestamp] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[role] WITH NOCHECK ADD
  CONSTRAINT [PK_role] PRIMARY KEY  CLUSTERED
  (
    [rid]
  )  ON [PRIMARY]
GO

ALTER TABLE [dbo].[watchdog] WITH NOCHECK ADD
   PRIMARY KEY  CLUSTERED
  (
    [wid]
  )  ON [PRIMARY]
GO

ALTER TABLE [dbo].[cache] WITH NOCHECK ADD
  CONSTRAINT [DF_cache_created] DEFAULT (0) FOR [created]
GO

 CREATE  INDEX [forum_tid_idx] ON [dbo].[forum]([tid]) ON [PRIMARY]
GO

ALTER TABLE [dbo].[node] WITH NOCHECK ADD
  CONSTRAINT [DF_node_score] DEFAULT (0) FOR [score],
  CONSTRAINT [DF_node_votes] DEFAULT (0) FOR [votes],
  CONSTRAINT [DF_node_uid] DEFAULT (0) FOR [uid],
  CONSTRAINT [DF_node_status] DEFAULT (1) FOR [status],
  CONSTRAINT [DF_node_created] DEFAULT (0) FOR [created],
  CONSTRAINT [DF_node_comment] DEFAULT (2) FOR [comment],
  CONSTRAINT [DF_node_promote] DEFAULT (0) FOR [promote],
  CONSTRAINT [DF_node_moderate] DEFAULT (0) FOR [moderate],
  CONSTRAINT [DF_node_changed] DEFAULT (0) FOR [changed],
  CONSTRAINT [DF_node_static] DEFAULT (0) FOR [static]
GO

 CREATE  INDEX [IX_role] ON [dbo].[role]([rid]) ON [PRIMARY]
GO

ALTER TABLE [dbo].[term_data] WITH NOCHECK ADD
  CONSTRAINT [DF_term_data_vid] DEFAULT (0) FOR [vid]
GO

ALTER TABLE [dbo].[users] WITH NOCHECK ADD
  CONSTRAINT [DF_users_mode] DEFAULT (0) FOR [mode],
  CONSTRAINT [DF_users_sort] DEFAULT (0) FOR [sort],
  CONSTRAINT [DF_users_threshold] DEFAULT (0) FOR [threshold],
  CONSTRAINT [DF_users_status] DEFAULT (0) FOR [status],
  CONSTRAINT [DF_users_timestamp] DEFAULT (0) FOR [timestamp],
  CONSTRAINT [DF_users_rid] DEFAULT (0) FOR [rid],
  CONSTRAINT [DF_users_pass] DEFAULT ('') FOR [pass],
  CONSTRAINT [DF_users_homepage] DEFAULT ('') FOR [homepage],
  CONSTRAINT [DF_users_theme] DEFAULT ('') FOR [theme],
  CONSTRAINT [DF_users_signature] DEFAULT ('') FOR [signature],
  CONSTRAINT [DF_users_language] DEFAULT ('') FOR [language]
GO

 CREATE  INDEX [IX_sid] ON [dbo].[sessions]([sid]) ON [PRIMARY]
GO

 CREATE  INDEX [IX_users] ON [dbo].[users]([uid]) ON [PRIMARY]
GO

 CREATE  INDEX [IX_timestamp] ON [dbo].[users]([timestamp]) ON [PRIMARY]
GO

 CREATE  INDEX [sessions_sid_idx] ON [dbo].[sessions]([sid]) ON [PRIMARY]
GO

 CREATE  INDEX [users_timestamp_idx] ON [dbo].[users]([timestamp]) ON [PRIMARY]
GO

ALTER TABLE [dbo].[watchdog] WITH NOCHECK ADD
  CONSTRAINT [DF__watchdog__uid__41EDCAC5] DEFAULT ('0') FOR [uid],
  CONSTRAINT [DF__watchdog__type__42E1EEFE] DEFAULT ('') FOR [type],
  CONSTRAINT [DF__watchdog__messag__43D61337] DEFAULT ('') FOR [message],
  CONSTRAINT [DF__watchdog__link__44CA3770] DEFAULT ('') FOR [link],
  CONSTRAINT [DF__watchdog__locati__45BE5BA9] DEFAULT ('') FOR [location],
  CONSTRAINT [DF__watchdog__hostna__46B27FE2] DEFAULT ('') FOR [hostname],
  CONSTRAINT [DF__watchdog__timest__47A6A41B] DEFAULT ('0') FOR [timestamp]
GO

--
-- Insert some default values
--

INSERT INTO system VALUES ('modules/admin.module','admin','module','',1);
INSERT INTO system VALUES ('modules/block.module','block','module','',1);
INSERT INTO system VALUES ('modules/comment.module','comment','module','',1);
INSERT INTO system VALUES ('modules/help.module','help','module','',1);
INSERT INTO system VALUES ('modules/node.module','node','module','',1);
INSERT INTO system VALUES ('modules/page.module','page','module','',1);
INSERT INTO system VALUES ('modules/story.module','story','module','',1);
INSERT INTO system VALUES ('modules/taxonomy.module','taxonomy','module','',1);
INSERT INTO system VALUES ('themes/marvin/marvin.theme','marvin','theme','Internet explorer, Netscape, Opera',1);

INSERT INTO variable(name,value) VALUES('update_start', 's:10:"2003-10-27";');
INSERT INTO variable(name,value) VALUES('theme_default','s:6:"marvin";');

INSERT INTO users(uid,name,mail,rid) VALUES(0,'','','1');

INSERT INTO blocks(module,delta,status,custom,region,weight,path) VALUES('user', 0, 1, 0, 1, 0, '');
INSERT INTO blocks(module,delta,status,custom,region,weight,path) VALUES('user', 1, 1, 0, 1, 0, '');


---
--- Functions. Functions first available in SQL Server 2000. GREATEST() used by forum.module, tracker.module at the moment.
---

GO
CREATE FUNCTION GREATEST (@a int, @b  int)
RETURNS int AS
BEGIN
  IF (@a > @b)
  BEGIN
    RETURN @a;
  END
  RETURN @b;
END
