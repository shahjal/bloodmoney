<?php

function block_help($section = "admin/help#block") {
  $output = "";

  switch ($section) {
    case 'admin/help#block':
      $output .= "<p>Blocks are the boxes visible in the sidebar(s) of your web site. These are usually generated automatically by modules (e.g. recent forum topics), but you can also create your own blocks using either static HTML or dynamic PHP content.</p>";
      $output .= "<p>The sidebar each block appears in depends on both which theme you're using (some are left-only, some right, some both), and on the settings in block management.</p><p>Whether a block is visible in the first place depends on three things:</p><ul><li>It must have its \"enabled\" box checked in block management.</li><li>If it has its \"custom\" box checked in block management, the user must have chosen to display it in their user preferences.</li><li>If the \"path\" field in block management is set, the visitor must be on a page that matches the path specification (more on this later).</li></ul>";
      $output .= "<p>The block management screen also lets you specify the vertical sort-order of the blocks within a sidebar. You do this by assigning a <b>weight</b> to each block. Lighter blocks (smaller weight) \"float up\" towards the top of the sidebar. Heavier ones \"sink down\" towards the bottom of it. Once you've positioned things just so, you can preview what the layout will look like in different types of themes by clicking the preview placement link in the menu to the left.</p>";
      $output .= "<p>The path setting lets you define the pages on which a specific block is visable. If you leave the path blank it will appear on all pages. The path uses a regular expression syntax so remember to escape special characters!</p>";
      $output .= "<p>In case you do not know what a regular expression is, you should read about them in the PHP manual. The chapter to look at is the one on %pcre.</p>";
      $output .= "<p>However, for basic tasks it is sufficient to look at the following examples:</p>";
      $output .= "<p>If the block should only show up on blog pages, use &lt;/blog&gt;.  To display on all node views use &lt;/node/view&gt;.  The angular brackets are used as delimiters of the regular expression.  To show up on either forum or book pages use &lt;(/forum|/book)&gt;.  The round brackets form a group of expressions, divided by the | character. It matches if any of the expressions in it match.  A more complicated example is &lt;/node/add/(story|blog|image)&gt;. Blocks which have their paths set to this expression will show up on story, block, or image composition pages.  If you want to show a block an all pages, but not the search page, use &lt;(^/$|[^(search)$/]+)&gt;</p>";
      $output .= "<h3>Custom Blocks</h3>";
      $output .= "<p>A custom block contains admin-supplied HTML, text or PHP content (as opposed to being generated automatically by a module). Each custom block consists of a title, a description, and a body containing text, HTML, or PHP code which can be as long as you wish. The Drupal engine will 'render' the content of the custom block.</p>";
      $output .= "<h4>PHP in custom blocks</h4>";
      $output .= "<p>If you know how to script in PHP, Drupal gives you the power to embed any script you like inside a block. It will be executed when the page is viewed and dynamically embedded into the page. This gives you amazing flexibility and power, but of course with that comes danger and insecurity if you don't write good code. If you are not familiar with PHP, SQL or with the site engine, avoid experimenting with PHP custom blocks because you can corrupt your database or render your site insecure or even unusable! If you don't plan to do fancy stuff with custom blocks then you're probably better off with straight HTML.</p>";
      $output .= "<p>Remember that the code within each PHP custom block must be valid PHP code - including things like correctly terminating statements with a semicolon so that the parser won't die. It is highly recommended that you develop your cusom blocks separately using a simple test script on top of a test database before migrating to your production environment.</p>";
      $output .= "<p>Notes:</p><ul><li>You can use global variables, such as configuration parameters, within the scope of a PHP box but remember that variables which have been given values in a PHP box will retain these values in the engine or module afterwards.</li><li>register_globals is now set to <b>off</b> by default. If you need form information you need to get it from the \"superglobals\" \$_POST, \$_GET, etc.</li><li>You can use the <code>return</code> statement to return the actual content for your block as well.</li></ul>";
      $output .= "<p>A basic example:</p>";
      $output .= "<blockquote><p>You want to have a box with the title \"Welcome\" that you use to greet your visitors. The content for this box could be created by going:</p>";
      $output .= "<pre>
  return t(\"Welcome visitor, ... welcome message goes here ...\");
</pre>";
      $output .= "<p>If we are however dealing with a registered user, we can customize the message by using:</p>";
      $output .= "<pre>
  if (\$user->uid) {
    return t(\"Welcome \$user->name, ... welcome message goes here ...\");
  }
  else {
    return t(\"Welcome visitor, ... welcome message goes here ...\");
  }";
      $output .= "</pre></blockquote>";
      $output .= "<p>For more in-depth examples, we recommend that you check the existing boxes and use them as a starting point.</p>";
      $output = t($output, array("%pcre" => "<a href=\"http://php.net/pcre/\">". t("Perl-Compatible Regular Expressions (PCRE)") ."</a>"));
      break;
    case 'admin/system/modules#description':
      $output = t("Controls the boxes that are displayed around the main content.");
      break;
    case 'admin/system/block':
      $output = t("Blocks are the boxes in the left- and right- side bars of the web site, depending on the choosen theme.  They are created by <b>active</b> Drupal modules.  In order to view a block it must be enabled. You can assign the block's placement by giving it a region and a weight. The region specifies which side of the page the block is on, and the weight sorts blocks within a region. Lighter (smaller weight value) blocks \"float up\" towards the top of the page. The path setting lets you define which pages you want a block to be shown on. The custom checkbox lets your users hide the block using their account setting. You can also create your own blocks, where you specify the content of the block rather than its being generated by a module (you can even use PHP in these). You can create one of these by clicking the %createblock link in the menu to the left. Edit and delete links will become active below for these customized blocks.", array("%createblock" => l(t("new block"), "admin/system/block/add")));
      break;
    case 'admin/system/block/add':
      $output = t("Here you can create a custom content block. Once you have created this block you must make it active, and give it a place on the page using %overview. The title is used when displaying the block. The description is used in the \"block\" column on the %overview page. If you are going to place PHP code in the block, and you have <b>create php content</b> permission (see the %permission page) you <b>must</b> change the type to PHP to make your code active.", array("%overview" => l(t("blocks"), "admin/system/block"), "%permission" => l(t("permissions"), "admin/user/permission")));
      break;
    case 'admin/system/block/preview':
      $output = t("This page show you the placement of your blocks in different themes types. The numbers are the weight of each block, which is used to sort them within the sidebars.");
      break;
  }

  return $output;
}

function block_perm() {
  return array("administer blocks");
}

function block_link($type) {
  if ($type == "system" && user_access("administer blocks")) {

    menu("admin/system/block", t("blocks"), "block_admin", 3);
    menu("admin/system/block/add", t("new block"), "block_admin", 2);
    menu("admin/system/block/preview", t("preview placement"), "block_admin", 3);
    menu("admin/system/block/help", t("help"), "block_help", 9);
  }
}

function block_block($op = "list", $delta = 0) {
  if ($op == "list") {
    $result = db_query("SELECT bid, title, info FROM {boxes} ORDER BY title");
    while ($block = db_fetch_object($result)) {
      $blocks[$block->bid]["info"] = $block->info;
    }
    return $blocks;
  }
  else {
    $block = db_fetch_object(db_query("SELECT * FROM {boxes} WHERE bid = %d", $delta));
    $data["subject"] = $block->title;
    $data["content"] = ($block->type == 1) ? eval($block->body) : $block->body;
    return $data;
  }
}

function block_admin_save($edit) {
  foreach ($edit as $module => $blocks) {
    foreach ($blocks as $delta => $block) {
      db_query("UPDATE {blocks} SET region = %d, status = %d, custom = %d, path = '%s', weight = %d WHERE module = '%s' AND delta = '%s'",
                $block["region"], $block["status"], $block["custom"], $block["path"], $block["weight"], $module, $delta);
    }
  }

  return t("the block settings have been updated.");
}

/**
 * update blocks db table with blocks currently exported by modules
 *
 * @param   array   $order_by   php array_multisort() style sort ordering, eg. "weight", SORT_ASC, SORT_STRING. see {@link http://www.php.net/manual/en/function.array-multisort.php}
 * @return  array   blocks currently exported by modules, sorted by $order_by
 * @access  private
 */
function _block_rehash($order_by = array("weight")) {
  $result = db_query("SELECT * FROM {blocks} ");
  while ($old_block = db_fetch_object($result)) {
    $old_blocks[$old_block->module][$old_block->delta] = $old_block;
  }

  db_query("DELETE FROM {blocks} ");

  foreach (module_list() as $module) {
    $module_blocks = module_invoke($module, "block", "list");
    if ($module_blocks) {
      foreach ($module_blocks as $delta => $block) {
        $block["module"] = $module;
        $block["delta"]  = $delta;
        if ($old_blocks[$module][$delta]) {
          $block["status"] = $old_blocks[$module][$delta]->status;
          $block["weight"] = $old_blocks[$module][$delta]->weight;
          $block["region"] = $old_blocks[$module][$delta]->region;
          $block["path"]   = $old_blocks[$module][$delta]->path;
          $block["custom"] = $old_blocks[$module][$delta]->custom;
        }
        else {
          $block["status"] = $block["weight"] = $block["region"] = $block["custom"] = 0;
          $block["path"]   = "";
        }

        // reinsert blocks into table
        db_query("INSERT INTO {blocks} (module, delta, status, weight, region, path, custom) VALUES ('%s', '%s', %d, %d, %d, '%s', %d)",
                  $block["module"], $block["delta"], $block["status"], $block["weight"], $block["region"], $block["path"], $block["custom"]);

        $blocks[] = $block;

        // build array to sort on
        $order[$order_by[0]][] = $block[$order_by[0]];
      }
    }
  }

  // sort
  array_multisort($order[$order_by[0]], $order_by[1] ? $order_by[1] : SORT_ASC, $order_by[2] ? $order_by[2] : SORT_REGULAR, $blocks);

  return $blocks;
}

function block_admin_display() {

  $blocks = _block_rehash();

  $header = array(t("block"), t("enabled"), t("custom"), t("weight"), t("region"), t("path"), array("data" => t("operations"), "colspan" => 2));

  foreach ($blocks as $block) {
    if ($block["module"] == "block") {
      $edit = l(t("edit"), "admin/system/block/edit/". $block["delta"]);
      $delete = l(t("delete"), "admin/system/block/delete/". $block["delta"]);
    }
    else {
      $edit = "";
      $delete = "";
    }

    $rows[] = array($block["info"], array("data" => form_checkbox(NULL, $block["module"]."][".$block["delta"]."][status", 1, $block["status"]), "align" => "center"), array("data" => form_checkbox(NULL, $block["module"]."][".$block["delta"]."][custom", 1, $block["custom"]), "align" => "center"), form_weight(NULL, $block["module"]."][".$block["delta"]."][weight", $block["weight"]), form_select(NULL, $block["module"]."][".$block["delta"]."][region", $block["region"], array(t("left"), t("right"))), form_textfield(NULL, $block["module"]."][".$block["delta"]."][path", $block["path"], 10, 255), $edit, $delete);
  }

  $output = table($header, $rows);
  $output .= form_submit(t("Save blocks"));

  return form($output);
}

function block_admin_preview() {

  $result = db_query("SELECT * FROM {blocks} WHERE status > 0 AND region = 0 ORDER BY weight");
  $lblocks .= "<table border=\"0\" cellpadding=\"2\" cellspacing=\"2\">\n";
  while ($block = db_fetch_object($result)) {
    $block_data = module_invoke($block->module, "block", "list");
    $name = $block_data[$block->delta]["info"];
    $lblocks .= " <tr><td>". ($block->status == 2 ? "<b>$name</b>" : $name) ."</td><td>$block->weight</td></tr>\n";
  }
  $lblocks .= "</table>\n";

  $result = db_query("SELECT * FROM {blocks} WHERE status > 0 AND region = 1 ORDER BY weight");
  $rblocks .= "<table border=\"0\" cellpadding=\"2\" cellspacing=\"2\">\n";
  while ($block = db_fetch_object($result)) {
    $block_data = module_invoke($block->module, "block", "list");
    $name = $block_data[$block->delta]["info"];
    $rblocks .= " <tr><td>". ($block->status == 2 ? "<b>$name</b>" : $name) ."</td><td>$block->weight</td></tr>\n";
  }
  $rblocks .= "</table>\n";

  $output .= "<h3>". t("Themes with both left and right sidebars") .":</h3>\n";
  $output .= "<table border=\"1\" cellpadding=\"2\" cellspacing=\"2\">\n";
  $output .= " <tr><td colspan=\"3\" style=\"text-align: center;\">". t("header") ."</td></tr>\n";
  $output .= " <tr><td>\n". ($lblocks ? $lblocks : "&nbsp;") ."</td><td style=\"width: 300px;\">&nbsp;</td><td>\n". ($rblocks ? $rblocks : "&nbsp;") ."</td></tr>\n";
  $output .= " <tr><td colspan=\"3\" style=\"text-align: center;\">". t("footer") ."</td></tr>\n";
  $output .= "</table>\n";

  $result = db_query("SELECT * FROM {blocks} WHERE status > 0 ORDER BY weight");
  $blocks .= "<table border=\"0\" cellpadding=\"2\" cellspacing=\"2\">\n";
  while ($block = db_fetch_object($result)) {
    $block_data = module_invoke($block->module, "block", "list");
    $name = $block_data[$block->delta]["info"];
    $blocks .= " <tr><td>". ($block->status == 2 ? "<b>$name</b>" : $name) ."</td><td>$block->weight</td></tr>\n";
  }
  $blocks .= "</table>\n";

  $output .= "<h3>". t("Themes with right-sidebar only") .":</h3>\n";
  $output .= "<table border=\"1\" cellpadding=\"2\" cellspacing=\"2\">\n";
  $output .= " <tr><td colspan=\"2\" style=\"text-align: center;\">". t("header") ."</td></tr>\n";
  $output .= " <tr><td style=\"width: 400px;\">&nbsp;</td><td>\n". ($blocks ? $blocks : "&nbsp;") ."</td></tr>\n";
  $output .= " <tr><td colspan=\"2\" style=\"text-align: center;\">". t("footer") ."</td></tr>\n";
  $output .= "</table>\n";

  $output .= "<h3>". t("Themes with left-sidebar only") .":</h3>\n";
  $output .= "<table border=\"1\" cellpadding=\"2\" cellspacing=\"2\">\n";
  $output .= " <tr><td colspan=\"2\" style=\"text-align: center;\">". t("header") ."</td></tr>\n";
  $output .= " <tr><td>\n". ($blocks ? $blocks : "&nbsp;") ."</td><td style=\"width: 400px;\">&nbsp;</td></tr>\n";
  $output .= " <tr><td colspan=\"2\" style=\"text-align: center;\">". t("footer") ."</td></tr>\n";
  $output .= "</table>\n";

  return $output;
}

function block_box_get($bid) {
  return db_fetch_array(db_query("SELECT * FROM {boxes} WHERE bid = %d", $bid));
}

function block_box_form($edit = array()) {
  $type = array(0 => "HTML", 1 => "PHP");

  $form = form_textfield(t("Title"), "title", $edit["title"], 50, 64);
  $form .= form_textfield(t("Description"), "info", $edit["info"], 50, 64);
  $form .= form_textarea(t("Body"), "body", $edit["body"], 70, 10);
  if (user_access("create php content")) {
    $form .= form_select(t("Type"), "type", $edit["type"], $type);
  }

  if ($edit["bid"]) {
    $form .= form_hidden("bid", $edit["bid"]);
  }

  $form .= form_submit(t("Save block"));

  return form($form);
}

function block_box_save($edit) {
  if (!user_access("create php content")) {
    $edit["type"] = 0;
  }

  if ($edit["bid"]) {
    db_query("UPDATE {boxes} SET title = '%s', body = '%s', info = '%s', type = %d WHERE bid = %d", $edit["title"], $edit["body"], $edit["info"], $edit["type"], $edit["bid"]);
    return t("the block has been updated.");
  }
  else {
    db_query("INSERT INTO {boxes} (title, body, info, type) VALUES  ('%s', '%s', '%s', %d)", $edit["title"], $edit["body"], $edit["info"], $edit["type"]);
    return t("the new block has been added.");
  }
}

function block_box_delete($bid) {
  if ($bid) {
    db_query("DELETE FROM {boxes} WHERE bid = %d", $bid);
    return t("the block has been deleted.");
  }
}

function block_admin() {
  $op = $_POST["op"];
  $edit = $_POST["edit"];

  if (user_access("administer blocks")) {

    if (empty($op)) {
      $op = arg(3);
    }

    switch ($op) {
      case "preview":
        $output = block_admin_preview();
        break;
      case "add":
        $output = block_box_form();
        break;
      case "edit":
        $output = block_box_form(block_box_get(arg(4)));
        break;
      case "delete":
        $output = status(block_box_delete(arg(4)));
        cache_clear_all();
        $output .= block_admin_display();
        break;
      case t("Save block"):
        $output = status(block_box_save($edit));
        cache_clear_all();
        $output .= block_admin_display();
        break;
      case t("Save blocks"):
        $output = status(block_admin_save($edit));
        cache_clear_all();
        // fall through
      default:
        $output .= block_admin_display();
    }

    return $output;
  }
  else {
    return message_access();
  }
}

function block_user($type, &$edit, &$user) {
  switch ($type) {
    case "register_form":
      $result = db_query("SELECT * FROM {blocks} WHERE custom = %d ORDER BY module, delta", 1);

      while ($block = db_fetch_object($result)) {
        $form .= form_hidden("block][$block->module][$block->delta", $block->status);
      }

      return $form;
    case "edit_form":
      $result = db_query("SELECT * FROM {blocks} WHERE custom = %d ORDER BY module, delta", 1);

      while ($block = db_fetch_object($result)) {
        $data = module_invoke($block->module, "block", "list");
        if ($data[$block->delta]["info"]) {
          $form .= "<tr><td>". $data[$block->delta]["info"] ."</td><td>". form_checkbox(NULL, "block][$block->module][$block->delta", 1, $user->block[$block->module][$block->delta]) ."</td></tr>\n";
        }
      }

      if (isset($form)) {
        return form_item(t("Block configuration"), "<table border=\"0\" cellpadding=\"2\" cellspacing=\"2\">". $form ."</table>", t("Enable the blocks you would like to see displayed in the side bars."));
      }

      break;
    case "edit_validate":
      if (!$edit["block"]) {
        $edit["block"] = array();
      }
      return $edit;
  }
}

?>
