<?php

function throttle_perm() {
  /*
  ** throttle module defines the following permissions:
  **   access throttle box          - see throttle statistics
  */
  return array("access throttle box");
}

function throttle_help($section = "admin/help#throttle") {

  switch ($section) {
    case "admin/system/modules#description":
      $output = t("Allows configuration of congestion control auto-throttle mechanism.");
      break;
    case "admin/system/modules/throttle":
      return t("If your site gets popular, or comes under a \"Denial of Service\" (DOS) attack, your hardware might become overwhelmed. These settings allow you to \"slow down\" the access to your site. To use throttle you need to have the %access enabled. For Drupal to preform throttling it needs to do an extra database query. This extra query happens on page displays. <b>Auto-throttle probability limiter</b> tells Drupal to do this extra DB query once every \"x\" page displays, where \"x\" is the percentage. So if it is set to 10%, the default, then for every 100 web pages it displays, it will preform the extra query ten time. ", array("%access" => l(t("access log"), "admin/system/modules/statistics")));
    case "admin/help#throttle":
      $output .= "<h3>Introduction</h3><p>This Drupal module allows you to enable and configure the auto-throttle congestion control mechanism offered by the %statistics-module.  The auto-throttle mechanism allows your site to automatically adapt to different server levels.</p>";
      $output .= "<p>This module also adds a block that displays the current status of the throttle.  You must have \"%throttle-block\" privileges to view the block.  As a general rule of thumb, only site administrators should be granted access to this block.</p>";
      $output .= "<p>The auto-throttle mechanism performs an extra database query in order to determine what the current throttle level should be.  Fortunately the throttle can be tuned so these database queries only occur on a fraction of all pages generated by your site, reducing the overhead to an insignificant amount.  Additionally, when the top-most throttle level is reached, all throttle queries are suspended for a configurable period of time.  More detail follows.</p>";
      $output .= "<p>As with any module, the throttle module needs to be %modules-enable before you can use it.  Also refer to the permissions section below if you wish to access the throttle statistics block.</p>";
      $output .= "<h3>Configuring the throttle module</h3><p>The %throttle-config for the throttle allows you to turn it on and off, as well as to fine-tune how sensitive it is.</p>";
      $output .= "<h4>enable auto-throttle:</h4><blockquote>This first option on the throttle module configuration screen allows you to enable or disable the auto-throttling mechanism.  Note that the access-log must also be enabled via the %statistics-config for the auto-throttling mechanism to have any effect.</blockquote>";
      $output .= "<h4>auto-throttle multiplier:</h4><blockquote><p>This second option allows you to tune the auto-throttle mechanism.  The auto-throttle mechanism supports six throttle levels, from 0 (off) to 5 (maximum).  The current throttle level is based upon how many pages have been accessed on your site in the past 60 seconds - the more pages being displayed, the higher the throttle level.  This multiplier defines how many hits are required to switch from one throttle level to the next.</p>";
      $output .= "<p>For example, with a throttle multiplier of 20:  Once 20 pages have been accessed on your site within a period of 60 seconds, the throttle level will be incremented to a level of 1.  Once 40 pages have been accessed on your site within a period of 60 seconds, the throttle level will be incremented to a level of 2.  And so on, until 100 pages are accessed on your site within a period of 60 seconds, at which time the throttle level will be set to a maximum level of 5.</p>";
      $output .= "<p>Upon reaching a throttle level of 5, access logs and the auto-throttle checking mechanism is automatically disabled.  It is only renabled by cron after a period of time defined by \"auto-throttle cron test\", explained below.</p></blockquote>";
      $output .= "<h4>auto-throttle probability limiter:</h4><blockquote><p>This option allows you to minimize the performance impact of the auto-throttle.  If we refer to the probability limiter as P, then P% of all pages generated by your site will perform an extra database query to verify that the current throttle level is appropriate to the current server load.</p>";
      $output .= "<p>As a rule of thumb, the higher your multiplier, the lower your probability limiter should be.  For example, if you have a multiplier of 100, then you logically don't need to check the throttle level more than once out of every 100 page views, so the probability limiter should be set to 1%.  As database queries are \"expensive\", it's recommended that you keep the probability limiter to the smallest percentage possible, while still high enough to react quickly to a change in server load.</p></blockquote>";
      $output .= "<h4>auto-throttle cron test:</h4><blockquote><p>The auto-throttle dynamically adjusts its level upward, but not downward.  That is to say, if you have a multiplier of 20 and you get 45 hits in one minute, your throttle level will be adjusted to a level of 2.  If a few minutes later you only get 35 hits in one minute, the throttle level will <b>NOT</b> be adjusted down to a level of 1.  This prevents the throttle from bouncing back and forth between two levels.</p>";
      $output .= "<p>In order for the throttle level to be dropped, \"cron.php\" must be called regularly.  This option then defines how often the level will be dropped by one to test the server load.  If the server load is no longer as high as it was, the level will stay where it is, until the cron test period passes again and cron drops the throttle level again.  This process repeats until the throttle is returned to a throttle level of 0.</p></blockquote>";
      $output .= "<h3>Throttle block</h3><p>This block displays some statistics regarding the current throttle and its configuration.  It is recommended that only site administrators receive the \"%throttle-access\" permission bit required to view this block.  It does not display information that would interest a normal site end-user.</p>";
      $output .= "<p>Don't forget to enable the block %throttle-block-enable.</p>";
      $output .= "<h3>Permissions</h3><p>This module has one permission that needs to be configured in %permissions.</p>";
      $output .= "<ul><li><i>access throttle block</i> - enable for user roles that get to view the throttle block.</li></ul>";
      $output .= "<h3>For programmers: throttle_status()</h3><p>The function <code>throttle_status()</code> will return a number from 0 to 5.  0 means that there is no throttle enabled at this time.  Each number above that is a progressively more throttled system...  To disable a feature when a site first begins to get busy, disable it at a throttle of 2 or 3.  To hold on to the bitter end, wait until 4 or 5.</p>";
      $output .= "<p>To implement the throttle, you should do something like this:";
      $output .= "<pre>       \$throttle = 0;
        /* verify that the statistics module is installed */
        if (function_exists(throttle_status) {
         \$throttle = throttle_status()
        }
       if (\$throttle >= \$my_throttle_value) {
          // throttle limit reached, disable stuff
        }
        else {
          // throttle limit not reached, execute normally
       }</pre></p>";
    $output = t($output, array("%statistics-module" => l(t("statistics module"), "admin/statistics"), "%throttle-block" => l(t("access throttle block"), "admin/user/permission"), "%modules-enable" => l(t("enabled"), "admin/system/modules"), "%throttle-config" => l(t("configuration section"), "admin/system/modules/throttle"), "%statistics-config" => l(t("statistics module"), "admin/system/modules/statistics"), "%throttle-access" => l(t("access throttle block"), "admin/user/permission"), "%throttle-block-enable" => l(t("here"), "admin/block"), "%permissions" => l(t("user permissions"), "admin/user/permission")));
    break;
  }

  return $output;
}


/* Adds configure option to the main configure site admin page */
function throttle_settings() {
  /* access log options */
  $output .= form_select(t("Enable auto-throttle"), "statistics_enable_auto_throttle", variable_get("statistics_enable_auto_throttle", 0), array("1" => t("enabled"), "0" => t("disabled")), "Enable auto-throttling congestion control mechanism.  Allows your site to adapt under extreme user loads.  Requires that 'access log' be enabled.");
  $throttles = array(1 => "1 (0,1,2,3,4,5)", 5 => "5 (0,5,10,15,20,25)", 10 => "10 (0,10,20,30,40,50)", 12 => "12 (0,12,24,36,48,60)", 15 => "15 (0,15,30,45,60,75)", 20 => "20 (0,20,40,60,80,100)", 30 => "30 (0,30,60,90,120,150)", 50 => "50 (0,50,100,150,200,250)", 60 => "60 (0,60,120,180,240,300)", 100 => "100 (0,100,200,300,400,500", 500 => "500 (0,500,1000,1500,2000,2500", 1000 => "1000 (0,1000,2000,3000,4000,5000)");
  $output .= form_select(t("Auto-throttle multiplier"), "statistics_throttle_multiplier", variable_get("statistics_throttle_multiplier", 60), $throttles, "Throttle tuning.  Specify how many hits in the past 60 seconds triggers higher throttle level.  Example: multiplier of 5 takes 5 hits for level 1, 25 hits for level 5.");
  $probabilities = array(0 => "100%", 1 => "50%", 2 => "33.3%", 3 => "25%", 4 => "20%", 5 => "16.6%", 7 => "12.5%", 9 => "10%", 19 => "5%", 99 => "1%", 199 => ".5%", 399 => ".25%", 989 => ".1%");
  $output .= form_select(t("Auto-throttle probability limiter"), "statistics_probability_limiter", variable_get("statistics_probability_limiter", 9), $probabilities, "Throttle tuning.  Probability based efficiency mechanism specifying the probability, expressed in a percentage of page views, an extra database query is performed to adjust throttle level.");
  $period = array(1800 => format_interval(1800), 3600 => format_interval(3600), 7200 => format_interval(7200), 10800 => format_interval(10800), 14400 => format_interval(14400), 18000 => format_interval(18000), 21600 => format_interval(21600), 43200 => format_interval(43200), 64800 => format_interval(64800), 86400 => format_interval(86400), 172800 => format_interval(172800), 259200 => format_interval(259200), 604800 => format_interval(604800));
  $output .= form_select(t("Auto-throttle cron test"), "statistics_throttle_cron_timer", variable_get("statistics_throttle_cron_timer", 10800), $period, "Throttle tuning.  Specify how often cron should drop the thottle level (if up) to test server load.  Requires cron.");
  return $output;
}


/* This displays admin oriented "Throttle status" block */
function throttle_display_throttle_block() {
  global $recent_activity;

  if (user_access("access throttle block")) {
    if (variable_get("statistics_enable_auto_throttle", 0)) {
      /* the throttle is enabled:  display the status of all throttle config */
      if (function_exists("throttle_status")) {
        $throttle = throttle_status();
      }
      else {
        $throttle = 0;
      }
      $multiplier = variable_get("statistics_throttle_multiplier", 60);
      $minimum = $throttle * $multiplier;
      $limiter = variable_get("statistics_probability_limiter", 9);
      /* calculate probability limiter's odds of updating throttle */
      $probability = substr((($limiter / ($limiter + 1) * 100) - 100) * -1, 0, 4);

      $output .= "Throttle: ". l("Enabled", "admin/system#statistics") ."<br />\n";
      if ($throttle < 5) {
        $maximum = (($throttle + 1) * $multiplier) - 1;
        $output .= "Current Level: $throttle ($minimum - $maximum)<br />\n";
      }
      else {
        $output .= "Current Level: $throttle ($minimum+)<br />\n";
      }
      $output .= "Probability: $probability%<br />\n";
      if ($recent_activity["hits"]) {
        $output .= "<br />This site has served ";
        $output .= format_plural($recent_activity["hits"] , "1 page", "%count pages");
        $output .= " in the past minute.";
      }
    }
    else {
      $output .= "Throttle: ". l("Disabled", "admin/system#statistics") ."<br />\n";
    }
  }
  return $output;
}

/* Block hook */
function throttle_block($op = "list", $delta = 0) {
  if ($op == "list") {
    $blocks[0]["info"] = t("Throttle status");
    return $blocks;
  }
  else {
    $block["subject"] = t("Throttle status");
    $block["content"] = throttle_display_throttle_block();
    return $block;
  }
}

?>
