char __rol(char a, char b)
{
    __asm
    {
        xor eax,eax
        mov al, a
        mov cl, b
        rol al, cl
    }

}

char __ror(char a, char b)
{
    __asm
    {
        xor eax, eax
        mov al, a
        mov cl, b
        ror al, cl
    }
}


void EncryptBuffer(char* input, char* output, int size)
{
    int i = 0;
    char internal_size = size - 1;

    do
    {
        output[size] = (input[i] ^ 0xc7) + __rol(1, (internal_size & 3)) + 1;
        internal_size += output[size];

        size--;
        i++;
    } while (size >= 0);
}


void DecryptBuffer(char* input, char* output, int size)
{
    int i = 0;
    char internal_size = size -1;

    do
    {
        output[i] = (input[size] - (__rol(1, (internal_size & 3)) + 1)) ^ 0xc7;
        internal_size += input[size];

        size--;
        i++;
    } while (size >= 0);
}