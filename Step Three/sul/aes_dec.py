import zlib
from Crypto import Random
from Crypto.Cipher import AES
from Crypto.Cipher import ARC4
import base64
import binascii

iv = binascii.a2b_hex("00000000000000000000000000000000")
aes_key = binascii.a2b_hex("726A5C7C475670706F6862567E465E5C")
data = open("stage_ghabli_2.bin", "rb").read()
cipher = AES.new(aes_key, AES.MODE_ECB, iv)
data_decrypted = cipher.decrypt(data)