import zlib
from Crypto import Random
from Crypto.Cipher import AES
import base64

data = open("raw.bin", "rb").read()
data_dec = zlib.decompress(data, 16+zlib.MAX_WBITS)
print data_dec

bit = base64.b64decode("3wkh8C3AU4tdKQ==")
key = base64.b64decode("vXc1ARd1dNWdFELzZ9TpfuEe8vYwLJcqMzluPHp42TE=")
iv = base64.b64decode("34VJcOLfchVZTFD/gPmwyQ==")
data_enc = open("solveme.gif", "rb").read()
cipher = AES.new(key, AES.MODE_ECB, iv )
data_decrypted = cipher.decrypt(data_enc)
file = open("dec.gif", "wb")
file.write(data_decrypted)
file.close()
print "end"
